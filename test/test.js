require('mocha')

const chai = require("chai");
const chaiHttp = require("chai-http");
const { it } = require('mocha');

chai.use(chaiHttp);

const { assert, expect, should, request } = chai;

//parameter pertama jelesin test apa, p2 jelasin unit apa
describe("REQRES API", function(){
    describe("endpoint unknown", function(){
        it("async", async function(){
            const res = await request("https://reqres.in/api")
            .get('/unknown');
            console.log(res)
            expect(res.body.support.url).to.be.equal("https://reqres.in/#support-heading'");
        });
        it("promise", function(){
            request("https://reqres.in/api")
            .post("/unknown")
            .end((err, res) => {
                expect(res.status).to.be.equal(201);
            });
        });
        
    })
}) 